from sre_constants import CHARSET


CHARSET_UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
CHARSET_LOWER = 'abcdefghijklmnopqrstuvwxyz'

class ROT13:
    def __init__(self):
        pass

    @staticmethod
    def encode(input, shift=13):
        cipher = ''
        for letter in input:
            if(letter != ' '):
                if (letter.isupper()):
                    cipher += CHARSET_UPPER[(ord(letter) - 65 + shift) % 26]
                else:
                    cipher += CHARSET_LOWER[(ord(letter) - 97 + shift) % 26]
            else:
                cipher += ' '

        return cipher

    @staticmethod
    def decode(input, shift=13):
        cipher = ''
        for letter in input:
            if(letter != ' '):
                if (letter.isupper()):
                    cipher += CHARSET_UPPER[(ord(letter) - 65 - shift) % 26]
                else:
                    cipher += CHARSET_LOWER[(ord(letter) - 97 - shift) % 26]
            else:
                cipher += ' '

        return cipher

if __name__ == "__main__":
    obj = ROT13()
    print("Starting the program.\n")
    input_str = str(input("Give a string: "))
    print(f'Original string: {input_str}')
    encode = obj.encode(input=input_str)
    decode = obj.decode(input=encode)
    print(f'Encoded: {encode}')
    print(f'Decoded: {decode}\n')
    print('Program ends.')
    
